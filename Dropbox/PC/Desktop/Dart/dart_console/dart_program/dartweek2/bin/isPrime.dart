import 'dart:io';

bool isPrime(int num) {
  bool Prime = false;
  for (int i = 2; i <= num / 2; ++i) {
    if (num % i == 0) {
      Prime = true;
      break;
    }
  }
  if (!Prime)
    return true;
  else
    return false;
}

void main() {
  int a = int.parse(stdin.readLineSync()!);
  print(isPrime(a));
}
