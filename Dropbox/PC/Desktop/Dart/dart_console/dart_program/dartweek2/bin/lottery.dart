import 'dart:io';

void checkWin(String lottery, int operator) {
  switch (operator) {
    case 1:
      {
        if (firstPrize(lottery)) {
          print(">>> Win first prize :D");
        } else {
          print(">>> You got no luck :(");
        }
      }
      break;
    case 2:
      {
        if (closeToFirstPrize(lottery)) {
          print(">>> Win close to first prize :D");
        } else {
          print(">>> You got no luck :(");
        }
      }
      break;
    case 3:
      {
        if (secondPrize(lottery)) {
          print(">>> Win second prize :D");
        } else {
          print(">>> You got no luck :(");
        }
      }
      break;
    case 4:
      {
        if (thirdPrize(lottery)) {
          print(">>> Win third prize :D");
        } else {
          print(">>> You got no luck :(");
        }
      }
      break;
    case 5:
      {
        if (lastTwoDigitsPrize(lottery)) {
          print(">>> Win last two digits prize :D");
        } else {
          print(">>> You got no luck :(");
        }
      }
      break;
    case 6:
      {
        if (firstThreeDigitsPrize(lottery)) {
          print(">>> Win first three digits prize :D");
        } else {
          print(">>> You got no luck :(");
        }
      }
      break;
    case 7:
      {
        if (lastThreeDigitsPrize(lottery)) {
          print(">>> Win last three digits prize :D");
        } else {
          print(">>> You got no luck :(");
        }
      }
      break;
  }
}

bool firstPrize(String lottery) {
  String firstPrize = "436594";
  if (lottery == firstPrize) {
    return true;
  }
  return false;
}

bool closeToFirstPrize(String lottery) {
  bool win = false;
  var nearfirstPrize = ["436593", "436595"];
  nearfirstPrize.forEach((element) {
    if (lottery == element) {
      win = true;
    }
  });
  return win;
}

bool secondPrize(String lottery) {
  bool win = false;
  var secondPrize = ["502412", "285563", "396501", "084971", "049364"];
  secondPrize.forEach((element) {
    if (lottery == element) {
      win = true;
    }
  });
  return win;
}

bool thirdPrize(String lottery) {
  bool win = false;
  var thirdPrize = [
    "996939",
    "043691",
    "422058",
    "853019",
    "662884",
    "166270",
    "666926",
    "896753",
    "242496",
    "575619"
  ];
  thirdPrize.forEach((element) {
    if (lottery == element) {
      win = true;
    }
  });
  return win;
}

bool lastTwoDigitsPrize(String lottery) {
  String twoRear = "14";
  if (lottery.substring(lottery.length - 2) == "14") {
    return true;
  }
  return false;
}

bool firstThreeDigitsPrize(String lottery) {
  bool win = false;
  var treeFront = ["893", "266"];
  treeFront.forEach((element) {
    if (lottery.substring(0, 3) == element) {
      win = true;
    }
  });
  return win;
}

bool lastThreeDigitsPrize(String lottery) {
  bool win = false;
  var treeRear = ["447", "282"];
  treeRear.forEach((element) {
    if (lottery.substring(lottery.length - 3) == element) {
      win = true;
    }
  });
  return win;
}

void main() {
  bool exit = false;
  while (exit == false) {
    print(
        'CHECK YOUR LOTTERY \nSelect the choice you want :\n1. Check first Prize\n2. Check close to first Prize\n3. Check second Prize\n4. Check third prize\n5. Check last 2 digits prize\n6. Check first 3 digits prize\n7. Check last 3 digits prize\n8. Exit\nInput choice number :');
    int operator = int.parse(stdin.readLineSync()!);
    if (operator > 8 || operator < 1) {
      print("Invalid choice");
    } else if (operator == 8) {
      exit = true;
    } else {
      print("Input your lottery number :");
      String lottery = stdin.readLineSync() as String;
      checkWin(lottery, operator);
    }
    print("Want to check again ? [ Y / N ] : ");
    String checkAgain = stdin.readLineSync() as String;
    if (checkAgain == "N") {
      exit = true;
    } else if (checkAgain == "Y") {
      exit = false;
    }
  }
}
